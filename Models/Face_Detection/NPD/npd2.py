import cv2
import h5py
import numpy as np
from NPDScan import NPDScan
from nms import nms
import sys
import time


minFace = 20
maxFace = 4000
overlap = 0.5
source=0
modelFile = 'model_frontal.mat'

frame_count = 0





f = h5py.File(modelFile,'r')
# x=f.get('npdModel')
# for n,v in x.items():
#     print n,np.array(v)
npdModel = {n:np.array(v) for n,v in f.get('npdModel').items()}
if __name__ == "__main__":
    
    cap = cv2.VideoCapture(0)
    hasFrame, frame = cap.read()
    vid_writer = cv2.VideoWriter('output-npd-{}.avi'.format(str(source).split(".")[0]),cv2.VideoWriter_fourcc('M','J','P','G'), 15, (frame.shape[1],frame.shape[0]))

    while(1):
        
        hasFrame, frame = cap.read()
        I = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        
    

    if not hasFrame:
        print("No frame")
    frame_count += 1
    startTime = time.time()
    rects = NPDScan(npdModel, I, minFace, maxFace)
    print('cost:',time.time() - startTime)
    numFaces = len(rects)
    print('%s faces detected.\n' % numFaces)
    rects = nms(rects,overlap)
    if numFaces > 0:
        for rect in rects:
            cv2.rectangle(frame, (rect[0], rect[1]), (rect[3], rect[4]), (0,255,0), 2)
            
        
    
    cv2.imshow("Face Detection Comparison", frame)
    vid_writer.write(frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        sys.exit()
       
    


    
    
cv2.destroyAllWindows()
vid_writer.release()

    
    

    
    








