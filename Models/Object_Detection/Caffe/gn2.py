import numpy as np
import argparse
import time
import cv2
import matplotlib.pyplot as plt


rows = open('/Users/aniketpihu/ml-workbench-v2/Models/Object_Detection/Caffe/synset_words.txt').read().strip().split("\n")

classes=[r[r.find(" ")+1:].split(',')[0] for r in rows]







cap = cv2.VideoCapture(0)
net = cv2.dnn.readNetFromCaffe('/Users/aniketpihu/ml-workbench-v2/Models/Object_Detection/Caffe/bvlc_googlenet.prototxt','/Users/aniketpihu/ml-workbench-v2/Models/Object_Detection/Caffe/bvlc_googlenet.caffemodel' )

while(True):
    # Capture frame-by-frame
    
    
    ret, image = cap.read()
    blob = cv2.dnn.blobFromImage(image, 1.0, (224, 224), (104, 117, 123),False,False)
    net.setInput(blob)
    detections = net.forward()

    for i in np.arange(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > 0.3:
            idx = int(detections[0, 0, i, 1])
            box = detections[0, 0, i, 3:7] * np.array([frameWidth, frameHeight, frameWidth, frameHeight])
            (startX, startY, endX, endY) = box.astype("int")
            label = "{}: {:.2f}%".format(classes[idx],
                confidence * 100)
            detected_objects.append(label)
            cv2.rectangle(frame, (startX, startY), (endX, endY),
                COLORS[idx], 2)
            y = startY - 15 if startY - 15 > 15 else startY + 15
            cv2.putText(frame, label, (startX, y),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
    cv2.imshow('frame',image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
