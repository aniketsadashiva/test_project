import numpy as np
import argparse
import time
import cv2
import matplotlib.pyplot as plt


rows = open('/Users/aniketpihu/ml-workbench-v2/Models/Object Detection/Caffe/synset_words.txt').read().strip().split("\n")

classes=[r[r.find(" ")+1:].split(',')[0] for r in rows]







cap = cv2.VideoCapture(0)
net = cv2.dnn.readNetFromCaffe('/Users/aniketpihu/ml-workbench-v2/Models/Object Detection/Caffe/bvlc_googlenet.prototxt','/Users/aniketpihu/ml-workbench-v2/Models/Object Detection/Caffe/bvlc_googlenet.caffemodel' )

while(True):
    # Capture frame-by-frame
    
    
    ret, image = cap.read()
    blob = cv2.dnn.blobFromImage(image, 1.0, (224, 224), (104, 117, 123),False,False)
    net.setInput(blob)
    preds = net.forward()

    idxs = np.argsort(preds[0])[::-1][:5]
    for (i, idx) in enumerate(idxs):
        if i == 0:
            text = "Label: {}, {:.2f}%".format(classes[idx],preds[0][idx] * 100)
            cv2.putText(image, text, (5, 25),  cv2.FONT_HERSHEY_SIMPLEX,0.7, (0, 0, 255), 2)
            

    # Our operations on the frame come here
    

    # Display the resulting frame
    cv2.imshow('frame',image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
