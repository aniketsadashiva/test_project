import cv2
import argparse
import numpy as np
import sys




def YOLOv2(net, frame):
    
    height,width,ch=frame.shape
    blob = cv2.dnn.blobFromImage(frame, 1.0/255.0, (416, 416), True, crop=False)

    net.setInput(blob)
    predictions = net.forward()
    probability_index=5
    bboxes = []
    for i in range(predictions.shape[0]):
        prob_arr=predictions[i][probability_index:]
        
        class_index=prob_arr.argmax(axis=0)
        
        confidence= prob_arr[class_index]
        
        if confidence > min_confidence:
            x_center=predictions[i][0]*width
            y_center=predictions[i][1]*height
            width_box=predictions[i][2]*width
            height_box=predictions[i][3]*height
            
            x1=int(x_center-width_box * 0.5)
            y1=int(y_center-height_box * 0.5)
            x2=int(x_center+width_box * 0.5)
            y2=int(y_center+height_box * 0.5)
            
            
            
            
            
            
            
            bboxes.append([x1, y1, x2, y2])
            cv2.rectangle(frame,(x1,y1),(x2,y2),(255,255,255),1)
            cv2.putText(frame,classes[class_index]+" "+"{0:.1f}".format(confidence),(x1,y1), cv2.FONT_HERSHEY_SIMPLEX, 1,(255,255,255),1,cv2.LINE_AA)
    return frame, bboxes





# Minimum confidence threshold. Increasing this will improve false positives but will also reduce detection rate.
min_confidence=0.14
model = 'yolov2.weights'
config = 'yolov2.cfg'


#Load names of classes
classes = None
with open('labels.txt', 'rt') as f:
    classes = f.read().rstrip('\n').split('\n')
print(classes)


net = cv2.dnn.readNetFromDarknet(config, model)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_DEFAULT)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)


source = 0
cap = cv2.VideoCapture(source)
hasFrame, frame = cap.read()
vid_writer = cv2.VideoWriter('output-dnn-{}.avi'.format(str(source).split(".")[0]),cv2.VideoWriter_fourcc('M','J','P','G'), 15, (frame.shape[1],frame.shape[0]))



frame_count = 0
tt_opencvDnn = 0

while True:
    hasFrame, frame = cap.read()
    if not hasFrame:
        break
    frame_count+=1
    
    
    outyolo, bboxes = YOLOv2(net,frame)
    cv2.imshow("Face Detection Comparison", outyolo)
    vid_writer.write(outyolo)
    
    if frame_count == 1:
        tt_opencvDnn = 0
        
    k = cv2.waitKey(10)
    if k == 27:
        break
    
    

        
    
cv2.destroyAllWindows()
vid_writer.release()

    
        
        
    

























