## ML-WORKBENCH

## Description
A python based GUI tool to enable qualitative apple-to-apple comparison between different models for different computer vision tasks on videos
Download weight files for YOLOv2,YOLOv3 and tiny YOLO v3 from:

YOLOv2:[YOLO v2 weights](https://drive.google.com/file/d/1UP2bFkRlsWhAe1I4vFYY6XMTjV9xpsD5/view?usp=sharing)
YOLOv3:[YOLO v3 weights](https://drive.google.com/file/d/1mDbli1PLJlzX-Ja7xHm3jY4xGApJ5tr_/view?usp=sharing)
Tiny YOLO v3:[Tiny YOLO v3 weights](https://drive.google.com/file/d/18qoK-foFcHY5kt8X2OkeSNDz_N0CouKt/view?usp=sharing)
Credits:https://pjreddie.com/darknet/yolo/ 



## Requirements
```bash
opencv-contrib-python==4.2.0.32
opencv-python==4.1.2.30
PyQt5==5.11.2
PyQt5-sip==4.19.19
imutils==0.5.3
numpy==1.18.2
```


## Installation
```bash
pip3 install -r requirements.txt
pip install --ignore-installed -r requirements.txt
```

## License
No Available

## References

[https://www.pyimagesearch.com/2018/02/26/face-detection-with-opencv-and-deep-learning/](https://www.pyimagesearch.com/2018/02/26/face-detection-with-opencv-and-deep-learning/)
[https://realpython.com/intro-to-python-threading/](https://realpython.com/intro-to-python-threading/)
[https://pjreddie.com/darknet/yolo/ ](https://pjreddie.com/darknet/yolo/ )
[https://www.learnopencv.com/face-detection-opencv-dlib-and-deep-learning-c-python/](https://www.learnopencv.com/face-detection-opencv-dlib-and-deep-learning-c-python/)
[Media Player using PyQt](https://stackoverflow.com/questions/57842104/how-to-play-videos-in-pyqt)

