from PyQt5 import QtCore, QtGui, QtWidgets
import cv2
import sys
from PyQt5.QtWidgets import  QWidget, QLabel, QApplication, QMainWindow,QFileDialog,QInputDialog
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap


class Thread(QThread):
    changePixmap = pyqtSignal(QImage)

    def run(self):
        global cap 
        
        cap= cv2.VideoCapture(0)
        while True:
            self.ret, self.frame = cap.read()
            if self.ret:
                # https://stackoverflow.com/a/55468544/6622587
                self.frame,self.bboxes=self.detectFaceOpenCVHaar(self.frame)
                self.rgbImage = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(self.rgbImage.data, w, h, bytesPerLine, QImage.Format_RGB888)
                self.p = convertToQtFormat.scaled(391, 221, Qt.KeepAspectRatio)
                self.changePixmap.emit(self.p)
                
                
                
                
    def detectFaceOpenCVHaar(self,frame, inHeight=300, inWidth=0):
        faceCascade=cv2.CascadeClassifier('/opt/anaconda3/pkgs/opencv-2.4.11-py27_1/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml ')
        test=faceCascade.load(('/opt/anaconda3/pkgs/opencv-2.4.11-py27_1/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml '))
        print(test)
        
        frameOpenCVHaar = frame
        frameHeight = frameOpenCVHaar.shape[0]
        frameWidth = frameOpenCVHaar.shape[1]
        if not inWidth:
            inWidth = int((frameWidth / frameHeight) * inHeight)
            
        scaleHeight = frameHeight / inHeight
        scaleWidth = frameWidth / inWidth

        frameOpenCVHaarSmall = cv2.resize(frameOpenCVHaar, (inWidth, inHeight))
        frameGray = cv2.cvtColor(frameOpenCVHaarSmall, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(frameGray)
        bboxes = []
        for (x, y, w, h) in faces:
            x1 = x
            y1 = y
            x2 = x + w
            y2 = y + h
            cvRect = [int(x1 * scaleWidth), int(y1 * scaleHeight),
                      int(x2 * scaleWidth), int(y2 * scaleHeight)]
            bboxes.append(cvRect)
            cv2.rectangle(frameOpenCVHaar, (cvRect[0], cvRect[1]), (cvRect[2], cvRect[3]), (0, 255, 0),
                          int(round(frameHeight / 150)), 4)
        return frameOpenCVHaar, bboxes







class Ui_MainWindow(QMainWindow):
    
    
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(843, 618)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.labelTask = QtWidgets.QLabel(self.centralwidget)
        self.labelTask.setGeometry(QtCore.QRect(20, 20, 41, 16))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.labelTask.setFont(font)
        self.labelTask.setObjectName("labelTask")
        self.Tasks_ComboBox = QtWidgets.QComboBox(self.centralwidget)
        self.Tasks_ComboBox.setGeometry(QtCore.QRect(60, 10, 341, 41))
        self.Tasks_ComboBox.setObjectName("Tasks_ComboBox")
        self.labelModel1 = QtWidgets.QLabel(self.centralwidget)
        self.labelModel1.setGeometry(QtCore.QRect(20, 60, 60, 16))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.labelModel1.setFont(font)
        self.labelModel1.setObjectName("labelModel1")
        self.Model1_ComboBox = QtWidgets.QComboBox(self.centralwidget)
        self.Model1_ComboBox.setGeometry(QtCore.QRect(80, 50, 251, 41))
        self.Model1_ComboBox.setObjectName("Model1_ComboBox")
        self.labelModel2 = QtWidgets.QLabel(self.centralwidget)
        self.labelModel2.setGeometry(QtCore.QRect(420, 50, 60, 16))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.labelModel2.setFont(font)
        self.labelModel2.setObjectName("labelModel2")
        self.Model2_ComboBox = QtWidgets.QComboBox(self.centralwidget)
        self.Model2_ComboBox.setGeometry(QtCore.QRect(520, 40, 291, 41))
        self.Model2_ComboBox.setObjectName("Model2_ComboBox")
        self.labelAnalysis_Rate = QtWidgets.QLabel(self.centralwidget)
        self.labelAnalysis_Rate.setGeometry(QtCore.QRect(420, 20, 91, 16))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.labelAnalysis_Rate.setFont(font)
        self.labelAnalysis_Rate.setObjectName("labelAnalysis_Rate")
        self.AnalysisRate_TextBox = QtWidgets.QTextBrowser(self.centralwidget)
        self.AnalysisRate_TextBox.setGeometry(QtCore.QRect(520, 20, 281, 20))
        self.AnalysisRate_TextBox.setObjectName("AnalysisRate_TextBox")
        self.VideoSource = QtWidgets.QLabel(self.centralwidget)
        self.VideoSource.setGeometry(QtCore.QRect(20, 100, 101, 21))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.VideoSource.setFont(font)
        self.VideoSource.setObjectName("VideoSource")
        self.WebCam_RadioButton = QtWidgets.QRadioButton(self.centralwidget)
        self.WebCam_RadioButton.setGeometry(QtCore.QRect(130, 100, 111, 21))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.WebCam_RadioButton.setFont(font)
        self.WebCam_RadioButton.setObjectName("WebCam_RadioButton")
        self.VideoFile_RadioButton = QtWidgets.QRadioButton(self.centralwidget)
        self.VideoFile_RadioButton.setGeometry(QtCore.QRect(280, 100, 111, 21))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.VideoFile_RadioButton.setFont(font)
        self.VideoFile_RadioButton.setObjectName("VideoFile_RadioButton")
        self.FileChooserButton = QtWidgets.QPushButton(self.centralwidget)
        self.FileChooserButton.setGeometry(QtCore.QRect(410, 90, 261, 41))
        self.FileChooserButton.setObjectName("FileChooserButton")
        self.FileChooserButton.clicked.connect(self.FileChooserButton_handler)

        self.genResultModel1Button = QtWidgets.QPushButton(self.centralwidget)
        self.genResultModel1Button.setGeometry(QtCore.QRect(10, 140, 311, 32))
        self.genResultModel1Button.setObjectName("genResultModel1Button")
        self.genResultModel2Button = QtWidgets.QPushButton(self.centralwidget)
        self.genResultModel2Button.setGeometry(QtCore.QRect(10, 190, 311, 32))
        self.genResultModel2Button.setObjectName("genResultModel2Button")
        self.progressBarModel1 = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBarModel1.setGeometry(QtCore.QRect(350, 140, 451, 31))
        self.progressBarModel1.setProperty("value", 24)
        self.progressBarModel1.setObjectName("progressBarModel1")
        self.progressBarModel2 = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBarModel2.setGeometry(QtCore.QRect(350, 190, 451, 31))
        self.progressBarModel2.setProperty("value", 24)
        self.progressBarModel2.setObjectName("progressBarModel2")
        self.VideoPlayer_Model1 = QtWidgets.QLabel(self.centralwidget)
        self.VideoPlayer_Model1.setEnabled(True)
        self.VideoPlayer_Model1.setGeometry(QtCore.QRect(20, 230, 391, 221))
        self.VideoPlayer_Model1.setObjectName("VideoPlayer_Model1")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(330, 530, 301, 291))
        self.label_2.setObjectName("label_2")
        self.PlayButton = QtWidgets.QPushButton(self.centralwidget)
        self.PlayButton.setGeometry(QtCore.QRect(290, 510, 113, 32))
        self.PlayButton.setObjectName("PlayButton")
        self.PauseButton = QtWidgets.QPushButton(self.centralwidget)
        self.PauseButton.setGeometry(QtCore.QRect(430, 510, 113, 32))
        self.PauseButton.setObjectName("PauseButton")
        self.VideoPlayer_Model2 = QtWidgets.QLabel(self.centralwidget)
        self.VideoPlayer_Model2.setEnabled(True)
        self.VideoPlayer_Model2.setGeometry(QtCore.QRect(430, 230, 391, 221))
        self.VideoPlayer_Model2.setObjectName("VideoPlayer_Model1_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 843, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        th = Thread(self)
        th.changePixmap.connect(self.setImage)
        th.start()
        
        
        
        
        
    @pyqtSlot(QImage)
    def setImage(self, image):
    
        self.VideoPlayer_Model1.setPixmap(QPixmap.fromImage(image))
        
    
    def FileChooserButton_handler(self):
         self.Open_File_Chooser_Dialog_box()
     
        
    def Open_File_Chooser_Dialog_box(self):
        filename=QFileDialog.getOpenFileName()
        print(filename)
     

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "ML Workbench Tool"))
        self.labelTask.setText(_translate("MainWindow", "Task:"))
        self.labelModel1.setText(_translate("MainWindow", "Model 1:"))
        self.labelModel2.setText(_translate("MainWindow", "Model 2:"))
        self.labelAnalysis_Rate.setText(_translate("MainWindow", "Analysis Rate:"))
        self.VideoSource.setText(_translate("MainWindow", "Video Source:"))
        self.WebCam_RadioButton.setText(_translate("MainWindow", "Web Cam"))
        self.VideoFile_RadioButton.setText(_translate("MainWindow", "Video File"))
        self.FileChooserButton.setText(_translate("MainWindow", "Open File"))
        self.genResultModel1Button.setText(_translate("MainWindow", "Generate Result For Model 1"))
        self.genResultModel2Button.setText(_translate("MainWindow", "Generate Result For Model 2"))
        self.VideoPlayer_Model1.setText(_translate("MainWindow", "TextLabel"))
        self.label_2.setText(_translate("MainWindow", "TextLabel"))
        self.PlayButton.setText(_translate("MainWindow", "Play"))
        self.PauseButton.setText(_translate("MainWindow", "Pause"))
        self.VideoPlayer_Model2.setText(_translate("MainWindow", "TextLabel"))
        
        
    
    




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
    cap.release()
    cv2.destroyAllWindows()
