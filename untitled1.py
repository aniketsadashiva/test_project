import sys
import cv2
from PyQt5.QtCore import QTimer,QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication,QDialog,QMainWindow,QWidget, QLabel,QFileDialog
from PyQt5.uic import loadUi
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QFont
from PyQt5 import QtCore, QtGui, QtWidgets
import dlib
import os
import cv2
import numpy as np
import imutils
from imutils.video import VideoStream
from imutils.video import FileVideoStream
from imutils.video import FPS
import time

#from VideoPlayer import VideoPlayer
from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import QDir, Qt, QUrl, QSize
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel, 
        QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget, QStatusBar,QMessageBox)

from datetime import datetime
import pandas




import json
global video_source
video_source=0

analysisrate=1
isPaused=0
Task=""
#Model 1 Parameters:
model1_Name=""
model1_configPath=""
model1_filePath=""
video_play_source_1=''
model1_labels_file=""

#Model 2 Parameters:
model2_Name=""
model2_configPath=""
model2_filePath=""
model2_labels_file=""
no_selected_1=1
no_selected_2=1








class MLWB(QMainWindow):
    
    
    def __init__(self):
        super(MLWB,self).__init__()
        loadUi('mlwbui.ui',self)
        self.image=None
        self.processedImage=None
        self.PlayButton.clicked.connect(self.PlayVideo)
        self.PauseButton.clicked.connect(self.PauseVideo)
        #self.detectButton.setCheckable(True)
        #self.detectButton.toggled.connect(self.detect_webcam_face)
        self.face_Enabled=False
        self.detected_objects = []
        self.FileChooserButton.setEnabled(False)
        self.FileChooserButton.clicked.connect(self.OpenFileDialog)

        self.model = QStandardItemModel()
        self.Tasks_ComboBox.setModel(self.model)
        self.Model1_ComboBox.setModel(self.model)
        self.Model2_ComboBox.setModel(self.model)
        self.model1_name=""
        self.model2_name=""
        self.model1_configpath=""
        self.model2_configpath=""
        self.model1_filepath=""
        self.model2_configpath=""
        self.filepath=""
        self.source=0
        self.WebCam_RadioButton.toggled.connect(self.Radiobutton_Toggled)
        self.VideoFile_RadioButton.toggled.connect(self.Radiobutton_Toggled)
        
        #self.faceCascade=''
        self.WebCam_RadioButton.setEnabled(False)
        self.task_selected=""
        self.video_writer1=""
        self.video_filename1=""
        self.video_writer2=""
        self.video_filename2=""
        self.genResultModel1Button.clicked.connect(self.generate_Result_Model_1_clicked)
        self.genResultModel2Button.clicked.connect(self.generate_Result_Model_2_clicked)
        self.genResultModel2Button.setEnabled(False)
        #self.genResultModel2Button.setEnabled(True)
    
        global analysisrate,video_play_source_1,video_play_source_2
        
        self.progressBarModel1.setValue(0)
        self.progressBarModel2.setValue(0)
        
        self.vp1=VideoPlayer1(self.centralwidget)
        self.vp1.resize(640,480)
        
        
        self.vp1.hide()
        self.vp1.setGeometry(QtCore.QRect(20, 230, 640, 480))
        #self.vp1.setFrameShape(QtWidgets.QFrame.Box)
        self.vp1.setObjectName("VideoPlayer1")
        self.vp2=VideoPlayer2(self.centralwidget)
        
        self.vp2.hide()
        self.vp2.setGeometry(QtCore.QRect(670, 230, 640, 480))
        #self.vp1.setFrameShape(QtWidgets.QFrame.Box)
        self.vp2.setObjectName("VideoPlayer2")
        
        
        self.vp1.mediaPlayer.positionChanged.connect(self.slider_pos_changed) 
        self.vp2.mediaPlayer.positionChanged.connect(self.slider_pos_changed)  
        self.vp1.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.vp2.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.vp1.positionSlider.sliderMoved.connect(self.set_slider_position)
        self.vp2.positionSlider.sliderMoved.connect(self.set_slider_position)

        
        

        


        
        with open('./config.json') as f:
            self.data=json.load(f)
            for c in self.data['tasks'] :
                task=QStandardItem(c['task_name'])
                self.model.appendRow(task)
                for model in c['model']:
                    model_name=QStandardItem(model['model_name'])
                    task.appendRow(model_name)
                
                
        self.Tasks_ComboBox.currentIndexChanged.connect(self.updateStateCombo)
        self.updateStateCombo(0)
        
        
        
        
    def slider_pos_changed(self,position):
        self.vp1.positionSlider.setValue(position)
        self.vp2.positionSlider.setValue(position)
        
        
    def set_slider_position(self,position):
        self.vp1.mediaPlayer.setPosition(position)
        self.vp2.mediaPlayer.setPosition(position)
        
        

    def mediaStateChanged(self, state):
        if self.vp1.mediaPlayer.state() == QMediaPlayer.PlayingState or self.vp1.mediaPlayer.state() == QMediaPlayer.PlayingState : 
            self.PauseButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPlay))
        else:
            self.PauseButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPause))

    def OpenFileDialog(self):
        global video_source
        filename = QFileDialog.getOpenFileName()
        video_source=filename[0]
        self.source = filename[0]
        print(self.source)

       

        
        
    def Radiobutton_Toggled(self):
        if self.WebCam_RadioButton.isChecked():
            #print("Wc")
            self.source=0
            global video_source
            
            
            
        if self.VideoFile_RadioButton.isChecked():
            self.FileChooserButton.setEnabled(True)
    
    
        

    def setImage1(self, image):
        
        self.VideoPlayer_Model1.setPixmap(QPixmap.fromImage(image))   
        

    def setImage2(self, image):
        
        self.VideoPlayer_Model2.setPixmap(QPixmap.fromImage(image).scaled(640,480,Qt.KeepAspectRatio))   
        
    
        
    def updateStateCombo(self, index):
        
        indx = self.model.index(index, 0, self.Tasks_ComboBox.rootModelIndex())
        self.Model1_ComboBox.setRootModelIndex(indx)
        self.Model1_ComboBox.setCurrentIndex(0) 
        self.Model2_ComboBox.setRootModelIndex(indx)
        self.Model2_ComboBox.setCurrentIndex(1) 
        self.Model1_ComboBox.currentIndexChanged.connect(self.model_selector)
        self.Model2_ComboBox.currentIndexChanged.connect(self.model_selector)
        
        
        


    ''' def detect_webcam_face(self,status):
            if status:
                self.detectButton.setText('Stop Detection')
                self.face_Enabled=True
            else:
                self.detectButton.setText('Detect Face') 
                self.face_Enabled=False
'''
    def PlayVideo(self):
        global video_writer1,video_writer2,no_selected_1,no_selected_2
        cv2.destroyAllWindows()
        if(no_selected_1==1):
            video_writer1.release()
        if(no_selected_2==1):
            video_writer2.release()
        
        self.VideoPlayer_Model1.hide()
        self.VideoPlayer_Model2.hide()
        self.vp1.show()
        self.vp2.show()
        
        global video_play_source_1,video_play_source_2,video_source,model1_Name,model2_Name
        video_play_source_1=os.getcwd()+str(('/output-{}-{}-{}.avi').format(model1_Name,str(self.source).split('/')[-1],int(analysisrate)))
        
        #print(video_play_source_1)
        video_play_source_2=os.getcwd()+str(('/output-{}-{}-{}.avi').format(model2_Name,str(self.source).split('/')[-1],int(analysisrate)))
        #print(video_play_source_2)
        
        '''self.Play_Video_Model_1_Thread=Thread_PlayVideo_1()
        self.Play_Video_Model_1_Thread.changePixmap.connect(self.setImage1)
        self.Play_Video_Model_2_Thread=Thread_PlayVideo_2()
        self.Play_Video_Model_2_Thread.changePixmap.connect(self.setImage2)
        
        self.Play_Video_Model_1_Thread.start()
        self.Play_Video_Model_2_Thread.start()'''
        
            
        self.vp1.playbuttonpressed()
        self.vp2.playbuttonpressed()
        
        
    
    
    
    
    def Analyze(self,source,model):
        msg=QMessageBox()
        msg.setWindowTitle("File Already Exists")
        msg.setText(("The file {} already Exists Do you want to Reanalyze?").format(source.split('/')[-1]))
                    
        msg.setIcon(QMessageBox.Question)
        msg.setStandardButtons(QMessageBox.Yes|QMessageBox.No)
        
        x=msg.exec_()
        if(x==16384 and model==1):
            global analysisrate
            
            analysisrate=self.AnalysisRate_TextBox.toPlainText()
        
           
        

            global video_play_source_1,video_play_source_2,video_source,model1_Name,model2_Name
            self.th1 = Thread_Model1(self)
            self.progressBarModel1.setRange(0,100)
            self.th1.valueChanged.connect(self.progressBarModel1.setValue)
            #self.label_edit_1()
            #self.th1.processFinished.connect(self.label_edit_1())
            self.th1.fps.connect(self.analyze_label_1_fps)
            self.th1.time.connect(self.analyze_label_1_time)
            video_source=self.source
            
            self.th1.changePixmap.connect(self.setImage1)
            self.th1.start()
            
        if(x==65536 and model==1):
            
            video_play_source_1=os.getcwd()+str(('/output-{}-{}-{}.avi').format(model1_Name,str(self.source).split('/')[-1],int(analysisrate)))
            
            self.VideoPlayer_Model1.hide()
            global no_selected_1
            no_selected_1=0
            self.vp1.show()
            self.genResultModel2Button.setEnabled(True)
                   
        if(x==65536 and model==2):
            
            video_play_source_2=os.getcwd()+str(('/output-{}-{}-{}.avi').format(model2_Name,str(self.source).split('/')[-1],int(analysisrate)))
            print("No" + video_play_source_2)
            self.VideoPlayer_Model2.hide()
            global no_selected_2
            no_selected_2=0
            self.vp2.show()

        if(x==16384 and model==2):
            
            
            analysisrate=self.AnalysisRate_TextBox.toPlainText()
        
            

            self.th2 = Thread_Model2(self)
            self.progressBarModel2.setRange(0,100)
            self.th2.valueChanged.connect(self.progressBarModel2.setValue)
            #self.label_edit_2()
            #self.th2.processFinished.connect(self.label_edit_2())
            self.th2.fps.connect(self.analyze_label_2_fps)
            self.th2.time.connect(self.analyze_label_2_time)
            
            self.th2.changePixmap.connect(self.setImage2)
            self.th2.start()
        

        
            
    def analyze_label_1_fps(self,value):
        self.progressBarModel1.setValue(100)
        self.fps_info_model_1.setText('Model 1 FPS: ' + str(value))
        self.VideoPlayer_Model1.hide()
        self.vp1.show()
        self.genResultModel2Button.setEnabled(True)
        
    def analyze_label_1_time(self,value):
        self.progressBarModel1.setValue(100)
        self.time_info_model_1.setText('Model 1 TIME: ' + str(value))
        self.VideoPlayer_Model1.hide()
        self.vp1.show()
        self.genResultModel2Button.setEnabled(True)
        
        
        
            
    def analyze_label_2_fps(self,value):
        self.progressBarModel2.setValue(100)
        self.fps_info_model_2.setText('Model 2 FPS: ' + str(value))
        self.VideoPlayer_Model2.hide()
        self.vp2.show()
        
    def analyze_label_2_time(self,value):
        self.progressBarModel2.setValue(100)
        self.time_info_model_2.setText('Model 2 TIME: ' + str(value))
        self.VideoPlayer_Model2.hide()
        self.vp2.show()
        
        
        
        
    
    '''def Msgbox_button_clicked(self,i):
        global flag,video_play_source_1,video_play_source_2
        if(i.text=='Yes'):
            self.flag1=0
            self.flag2=0
            print(i.text)
            
            
            

        if(i.text=='No'):
            self.flag1=1
            self.flag2=1
            print(i.text)'''
    def generate_Result_Model_1_clicked(self):
        #self.capture=cv2.VideoCapture(self.source)
        #self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT,480)
        #self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
        self.vp1.hide()
        self.VideoPlayer_Model1.show()
        global analysisrate
        self.flag1=0
        analysisrate=self.AnalysisRate_TextBox.toPlainText()
        
        global video_source
        
            
    
        

        global video_play_source_1,video_play_source_2,video_source,model1_Name,model2_Name
        video_play_source_1=os.getcwd()+str(('/output-{}-{}-{}.avi').format(model1_Name,str(self.source).split('/')[-1],int(analysisrate)))
        if(os.path.exists(video_play_source_1)):
            
            self.flag1=1
            self.Analyze(source=video_play_source_1,model=1)

        else:
            self.th1 = Thread_Model1(self)
            self.progressBarModel1.setRange(0,100)
            self.th1.valueChanged.connect(self.progressBarModel1.setValue)

            self.th1.processFinished.connect(self.label_edit_1)
            video_source=self.source
            
            self.th1.changePixmap.connect(self.setImage1)
            self.th1.start()


        
        #isPaused=0
        #self.PauseButton.setEnabled(True)
        #srcc1="/Users/aniketpihu/Downloads/bionic.mp4"
     


        k = cv2.waitKey(0)
        if k == 27:         # wait for ESC key to exit
            cv2.destroyAllWindows()
            
    def label_edit_1(self,value=100):
        global fps_1,time_1
       
        if(value ==100):
            self.progressBarModel1.setValue(100)
            self.genResultModel2Button.setEnabled(True)
            self.VideoPlayer_Model1.hide()
            self.fps_info_model_1.setText('Model 1 FPS: ' + str(fps_1))
            self.time_info_model_1.setText('Model 1 TIME: ' + str(time_1))
            self.VideoPlayer_Model1.hide()
            self.vp1.show()
        
            
            

            
            
        
            
            

    def generate_Result_Model_2_clicked(self):
        #self.capture=cv2.VideoCapture(self.source)
        #self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT,480)
        #self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
        self.vp2.hide()
        self.VideoPlayer_Model2.show()
        global analysisrate
        
        analysisrate=self.AnalysisRate_TextBox.toPlainText()
        
        
        global video_source,video_play_source_1,video_play_source_2,video_source,model1_Name,model2_Name
        video_play_source_2=os.getcwd()+str(('/output-{}-{}-{}.avi').format(model2_Name,str(self.source).split('/')[-1],int(analysisrate)))
               
        if(os.path.exists(video_play_source_2)):
           
            
            self.Analyze(video_play_source_2,model=2)

        else:
        
            self.th2 = Thread_Model2(self)
        
            self.th2.valueChanged.connect(self.progressBarModel2.setValue)
            self.th2.processFinished.connect(self.label_edit_2)
          
        #self.PauseButton.setEnabled(True)
        #srcc1="/Users/aniketpihu/Downloads/bionic.mp4"
            video_source=self.source
            
            self.th2.changePixmap.connect(self.setImage2)
            self.th2.start()
        


        k = cv2.waitKey(0)
        if k == 27:         # wait for ESC key to exit
            cv2.destroyAllWindows()
    def label_edit_2(self,value=100):
        global fps_2,time_2
        #print("label 2")
        
        if(value == 100):
            self.PlayButton.show()
            self.progressBarModel2.setValue(100)
            self.fps_info_model_2.setText('Model 2 FPS: ' + str(fps_2))
            self.time_info_model_2.setText('Model 2 TIME: ' + str(time_2))
            self.VideoPlayer_Model2.hide()
            self.vp2.show()
        
            
    def update_frame(self):
        ret,self.image=self.capture.read()
        
        self.image=cv2.flip(self.image,1)
        
        
        
        
        detected_image_1=self.detect_face_model1(self.image)
        self.video_writer1=cv2.VideoWriter('output-{}-{}.avi'.format(self.model1_name,str(self.source).split(".")[0]),cv2.VideoWriter_fourcc('M','J','P','G'), 15, (detected_image_1.shape[1],detected_image_1.shape[0]))
        self.video_writer1.write(detected_image_1)
        self.displayImage(detected_image_1,1)
        detected_image_2=self.detect_face_model2(self.image)
        self.video_writer2=cv2.VideoWriter('output-{}-{}.avi'.format(self.model2_name,str(self.source).split(".")[0]),cv2.VideoWriter_fourcc('M','J','P','G'), 15, (detected_image_1.shape[1],detected_image_1.shape[0]))
        self.video_writer2.write(detected_image_2)
        self.displayImage(detected_image_2,2)
            
    #To select Models from ComboBox
    def model_selector(self,i):
        global Task,model1_Name,model2_Name,model1_filePath,model1_configPath,model2_filePath,model2_configPath,model1_labels_file,model2_labels_file
        Task=self.Tasks_ComboBox.currentText()
        model1=self.Model1_ComboBox.currentText()
        model2=self.Model2_ComboBox.currentText()
        for tasks in self.data['tasks']:
            if tasks['task_name'] == Task :
                #Updating Model 1 parameters
                for item in tasks['model']:
                    if item['model_name']==model1:
                        model1_Name=model1
                        model1_filePath=os.getcwd()+item['model_path']
                        model1_configPath=os.getcwd()+item['model_config_path']
                        model1_labels_file=os.getcwd()+item['model_labels_file']
                        #print(model1_Name)
                        #print(model1_configPath)
                
                        
                
                #Updating Model 2 parameters
                for item in tasks['model']:
                    if item['model_name']==model2:
                        model2_Name=model2
                        model2_filePath=os.getcwd()+item['model_path']
                        model2_configPath=os.getcwd()+item['model_config_path']
                        #print(model2_Name)
                        #print(model2_configPath)
                        model2_labels_file=os.getcwd()+item['model_labels_file']
                        #print(self.model2_configfile)
                        
                    
    
    
    
    
    def PauseVideo(self):
        

        if self.vp1.mediaPlayer.state() == QMediaPlayer.PlayingState or self.vp1.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.vp1.mediaPlayer.pause()
            self.vp2.mediaPlayer.pause()
            
        else:
            self.vp1.mediaPlayer.play()
            self.vp2.mediaPlayer.play()



    def displayImage(self,img,window=1):
       pass
            
            
            
           
            
            
            
            

class Thread_Model1(QThread):
    changePixmap = pyqtSignal(QImage)
    global video_source
    f=0;
    valueChanged = QtCore.pyqtSignal(int)
    processFinished=QtCore.pyqtSignal(int)
    fps=    QtCore.pyqtSignal(float)
    time=    QtCore.pyqtSignal(float)

    
    def run(self):
        
        
        framecount = 0
        
    
        global isPaused,analysisrate,Task,model1_Name,model1_configPath,model1_filePath,model2_Name,model2_configPath,model2_filePath,video_writer1
        self.model1_name=model1_Name
        self.model2_name=model2_Name
        self.task=Task
        
        self.initializeModel()
        self.capture = cv2.VideoCapture(video_source)
        #print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {}".format(self.capture.get(cv2.CAP_PROP_FPS)))
        #print(self.capture.get(cv2.CAP_PROP_FRAME_COUNT))
        #starttime = time.time()
        ret, self.frame = self.capture.read()
        video_writer1=cv2.VideoWriter('output-{}-{}-{}.avi'.format(model1_Name,str(video_source).split('/')[-1],int(analysisrate)),cv2.VideoWriter_fourcc('M','J','P','G'),int(analysisrate), (self.frame.shape[1],self.frame.shape[0]))
        fvs = FileVideoStream(video_source).start()
        #time.sleep(1.0)
        fps = FPS().start()
        self.initializeModel()
        while self.capture.isOpened() and  fvs.running():
            self.processFinished.emit(0)
            frame = fvs.read()
            if frame is None:
                
                break
            #frame = imutils.resize(frame, width=450)
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame = np.dstack([frame, frame, frame])
            
            ret, self.frame = self.capture.read(framecount)
            totalFrames =self.capture.get(cv2.CAP_PROP_FRAME_COUNT)
            if framecount <= int(self.capture.get(cv2.CAP_PROP_FRAME_COUNT)):
                #ret, self.frame = self.capture.read(framecount)
                self.capture.set(cv2.CAP_PROP_POS_FRAMES, framecount)
                success, self.frame = self.capture.read()

                framecount += int(analysisrate)
                rgbImage = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                self.ret_img1=rgbImage
                    
              
                
                    
                self.ret_img1=self.Analyze_model1(rgbImage)
                        
                    
                    
                    
                    
                bgrImage=   cv2.cvtColor(self.ret_img1, cv2.COLOR_RGB2BGR)
                video_writer1.write(bgrImage)
                convertToQtFormat = QImage(self.ret_img1.data, w, h, bytesPerLine, QImage.Format_RGB888)
                p = convertToQtFormat.scaled(640, 480)
                    
                
                self.changePixmap.emit(p)
               
                complete = 100*(framecount/totalFrames)
              
                
                
                self.valueChanged.emit(complete)
                    
                    
                
                    
                    
                k = cv2.waitKey(0)
                if k == 27:  
                    cv2.destroyAllWindows()
                        
            cv2.waitKey(1)
            fps.update()
            
        
        fps.stop()
        self.processFinished.emit(100)
        global fps_1,time_1
        fps_1=fps.fps()
        time_1=fps.elapsed()
        self.fps.emit(fps_1)
        self.time.emit(time_1)
        print("[INFO] elasped time: {:.2f} (Model 1)".format(fps.elapsed()))
        print("[INFO] approx. FPS: {:.2f}(Model 1)".format(fps.fps()))
        cv2.destroyAllWindows()
        fvs.stop()
    def initializeModel(self):
        global isPaused,analysisrate,Task,model1_Name,model1_configPath,model1_filePath,model2_Name,model2_configPath,model2_filePath,model1_labels_file
        #print(model1_Name)
        file_path=model1_filePath
        configFile=model1_configPath
        modelFile=model1_filePath
        
        if model1_Name=="Haar Cascade":
            
            self.faceCascade=cv2.CascadeClassifier(file_path)
            
            
        if model1_Name=="MMOD Dlib" :
            
            self.detector_dlib_mmod=dlib.cnn_face_detection_model_v1(file_path)
            
        if  model1_Name=="HOG Dlib":    
            global detector_dlib_hog
            
            self.detector_dlib_hog=dlib.get_frontal_face_detector()
            
            
        if model1_Name=="Resnet SSD" or model1_Name=="MTCNN" or model1_Name=="Mobile Net SSD" or  model1_Name=="Google Net":
            global net
            self.net=cv2.dnn.readNetFromCaffe(configFile, modelFile)

            
        if model1_Name=="YOLO v3":
            
            self.net_yolov3 = cv2.dnn.readNet(modelFile, configFile)
        
           
        if model1_Name=="YOLO v2":
            
            
            self.net_yolov2=cv2.dnn.readNetFromDarknet(configFile, modelFile)
            
           
        if model1_Name=="Tiny YOLO":
            
            
            self.net_tiny_yolo=cv2.dnn.readNet(configFile, modelFile)
        if model1_Name=="Background Subtractor":
            
            self.fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()

        if model1_Name=="Adaptive Gaussian" :
            
            self.fgbgAdaptiveGaussain = cv2.createBackgroundSubtractorMOG2()
        if model1_Name=="Bayesian Segmentation":
            self.kernel,self.fgbgBayesianSegmentation
                    
            self.kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
            self.fgbgBayesianSegmentation = cv2.bgsegm.createBackgroundSubtractorGMG()
            
            
            
            
            
       
  
    
               
                    
    def Analyze_model1(self,img):
        
        ret_img=img
        
        global isPaused,analysisrate,Task,model1_Name,model1_configPath,model1_filePath,model2_Name,model2_configPath,model2_filePath,model1_labels_file
        #print(model1_Name)
        if model1_Name=="Haar Cascade":
            
            
            
            
            ret_img=self.Haar_Cascade(img,file_path=model1_filePath)
            
        if model1_Name=="MMOD Dlib" or model1_Name=="HOG Dlib":
            ret_img=self.Dlib(img,file_path=model1_filePath)
            
            
        if model1_Name=="Resnet SSD":
            ret_img=self.Caffe(img,configFile=model1_configPath,modelFile=model1_filePath)
        if model1_Name=="MTCNN":
            ret_img=self.Caffe(img,configFile=model1_configPath,modelFile=model1_filePath)
            
        if model1_Name=="Mobile Net SSD":
            ret_img=self.Caffe(img,configFile=model1_configPath,modelFile=model1_filePath)
            
        if model1_Name=="Google Net":
            ret_img=self.Caffe(img,configFile=model1_configPath,modelFile=model1_filePath,labelsfile=model1_labels_file)
            
        if model1_Name=="YOLO v3":
            ret_img=self.YOLOv3(img,configFile=model1_configPath,modelFile=model1_filePath,labelsfile=model1_labels_file)
        if model1_Name=="YOLO v2" :
             ret_img=self.YOLOv2(img,configFile=model1_configPath,modelFile=model1_filePath,labelsfile=model1_labels_file)
        if model1_Name=="Tiny YOLO" :
             ret_img=self.tinyYOLO(img,configFile=model1_configPath,modelFile=model1_filePath,labelsfile=model1_labels_file)
        if  model1_Name=="Background Subtractor" or model1_Name=="Adaptive Gaussian" or model1_Name=="Bayesian Segmentation":
            ret_img=self.MotionDetection(img) 
      
    
            
        return ret_img
  
    

                
                
    def Haar_Cascade(self,img,file_path):
        
        #modelfilee='/Users/aniketpihu/ml-workbench-v2/Models/Face_Detection/models/haarcascade_frontalface_default.xml'
        
        
         
        after_image=img.copy()
        gray=cv2.cvtColor((after_image),cv2.COLOR_BGR2GRAY)
        faces=self.faceCascade.detectMultiScale(gray,1.2,5,minSize=(90,90))

        for(x,y,w,h) in faces:
            cv2.rectangle(after_image,(x,y),(x+w,y+h),(255,0,0),2)

        return after_image
    
    
    def Dlib(self, img, inHeight=300, inWidth=0,file_path=""):
        mymodel=""
        
        if self.model1_name=="MMOD Dlib" or self.model2_name=="MMOD Dlib":
            detector=self.detector_dlib_mmod
     
            
        if self.model1_name=="HOG Dlib" or self.model2_name=="HOG Dlib":
            mymodel="HOG Dlib"
            detector=dlib.get_frontal_face_detector()

        frameDlib = img.copy()
        frameHeight = frameDlib.shape[0]
        frameWidth = frameDlib.shape[1]
        if not inWidth:
            inWidth = int((frameWidth / frameHeight)*inHeight)

        scaleHeight = frameHeight / inHeight
        scaleWidth = frameWidth / inWidth

        frameDlibSmall = cv2.resize(frameDlib, (inWidth, inHeight))

        frameDlibSmall = cv2.cvtColor(frameDlibSmall, cv2.COLOR_BGR2RGB)
        
        faceRects = detector(frameDlibSmall, 0)

        #print(frameWidth, frameHeight, inWidth, inHeight)
        bboxes = []
        if mymodel=="HOG Dlib":
            for faceRect in faceRects:
                cvRect = [int(faceRect.left()*scaleWidth), int(faceRect.top()*scaleHeight),
                      int(faceRect.right()*scaleWidth), int(faceRect.bottom()*scaleHeight) ]
                bboxes.append(cvRect)
                #print(bboxes)
                cv2.rectangle(frameDlib, (cvRect[0], cvRect[1]), (cvRect[2], cvRect[3]), (255, 0, 0),int(round(frameHeight/150)), 2)
                
                
                
        else:
                
                for faceRect in faceRects:
                    cvRect = [int(faceRect.rect.left()*scaleWidth), int(faceRect.rect.top()*scaleHeight),
                              int(faceRect.rect.right()*scaleWidth), int(faceRect.rect.bottom()*scaleHeight) ]
                    bboxes.append(cvRect)
                    #print(bboxes)
                    cv2.rectangle(frameDlib, (cvRect[0], cvRect[1]), (cvRect[2], cvRect[3]), (255, 0, 0),int(round(frameHeight/150)), 2)
        
        
        
        
        return frameDlib
    
    
     
    
    
    
    
    def Caffe(self,frame,configFile,modelFile,labelsfile="" ):
        net=self.net
        #if (self.task=="Face Detection" and (self.model1_name=="MTCNN" or self.model2_name=="MTCNN")):
        # initialize the list of class labels MobileNet SSD was trained to
        # detect, then generate a set of bounding box colors for each class
        CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
                   "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
                   "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
                   "sofa", "train", "tvmonitor"]
        COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

        
        
        frameOpencvDnn = frame.copy()
        frameHeight = frameOpencvDnn.shape[0]
        frameWidth = frameOpencvDnn.shape[1]
        detected_objects = []
        
        if self.model1_name=='MTCNN':
            blob = cv2.dnn.blobFromImage(frameOpencvDnn, 1.0, (300, 300), [104, 117, 123], False, False)
            net.setInput(blob)
            detections = net.forward()
            bboxes = []
            for i in range(detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                if confidence > 0.9:
                    x1 = int(detections[0, 0, i, 3] * frameWidth)
                    y1 = int(detections[0, 0, i, 4] * frameHeight)
                    x2 = int(detections[0, 0, i, 5] * frameWidth)
                    y2 = int(detections[0, 0, i, 6] * frameHeight)
                    bboxes.append([x1, y1, x2, y2])
                    cv2.rectangle(frameOpencvDnn, (x1, y1), (x2, y2), (0, 255, 0), int(round(frameHeight/150)), 8)
                
                
        
        if self.model1_name=='Mobile Net SSD':
           
            blob = cv2.dnn.blobFromImage(cv2.resize(frameOpencvDnn, (300, 300)),0.007843, (300, 300), 127.5)
            net.setInput(blob)
            detections = net.forward()
            for i in np.arange(0, detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                if confidence > 0.3:
                    idx = int(detections[0, 0, i, 1])
                    box = detections[0, 0, i, 3:7] * np.array([frameWidth, frameHeight, frameWidth, frameHeight])
                    (startX, startY, endX, endY) = box.astype("int")
                    label = "{}: {:.2f}%".format(CLASSES[idx],confidence * 100)
                
                    detected_objects.append(label)
                    cv2.rectangle(frameOpencvDnn, (startX, startY), (endX, endY), COLORS[idx], 2)
                                         
                    y = startY - 15 if startY - 15 > 15 else startY + 15
                    if self.task=="Human Detection":
                        if label=="person":
                            cv2.putText(frameOpencvDnn, label, (startX, y),cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                            
                    else:
                        cv2.putText(frameOpencvDnn, label, (startX, y),cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                    print(label)
                                
            
        if self.model1_name=='Google Net':
           
            rows = open(labelsfile).read().strip().split("\n")

            classes=[r[r.find(" ")+1:].split(',')[0] for r in rows]
            blob = cv2.dnn.blobFromImage(frameOpencvDnn, 1.0, (224, 224), (104, 117, 123),False,False)
            net.setInput(blob)
            preds = net.forward()

            idxs = np.argsort(preds[0])[::-1][:5]
            for (i, idx) in enumerate(idxs):
                if i == 0:
                    text = "Label: {}".format(classes[idx])
                    cv2.putText(frameOpencvDnn, text, (5, 25),  cv2.FONT_HERSHEY_SIMPLEX,0.7, (0, 0, 255), 2)
            




                
        return frameOpencvDnn
        

    def YOLOv3(self,frame,configFile,modelFile,labelsfile="" ):
        yolo_img=frame.copy()
        height, width, channels = yolo_img.shape
        
        
        classes = []
        with open(labelsfile, "r") as f:
            classes = [line.strip() for line in f.readlines()]
            
            
        layers_names = self.net_yolov3.getLayerNames()
        output_layers = [layers_names[i[0]-1] for i in self.net_yolov3.getUnconnectedOutLayers()]
        colors = np.random.uniform(0, 255, size=(len(classes), 3))
        
        blob = cv2.dnn.blobFromImage(yolo_img, scalefactor=0.00392, size=(320, 320), mean=(0, 0, 0), swapRB=True, crop=False)
        
        self.net_yolov3.setInput(blob)
        outputs = self.net_yolov3.forward(output_layers)
        boxes = []
        confs = []
        class_ids = []
        for output in outputs:
            for detect in output:
                scores = detect[5:]
                class_id = np.argmax(scores)
                conf = scores[class_id]
                if conf > 0.3:
                    center_x = int(detect[0] * width)
                    center_y = int(detect[1] * height)
                    w = int(detect[2] * width)
                    h = int(detect[3] * height)
                    x = int(center_x - w/2)
                    y = int(center_y - h / 2)
                    boxes.append([x, y, w, h])
                    confs.append(float(conf))
                    class_ids.append(class_id)

        
    
    
    
        indexes = cv2.dnn.NMSBoxes(boxes, confs, 0.5, 0.4)
        font = cv2.FONT_HERSHEY_PLAIN
        for i in range(len(boxes)):
            if i in indexes:
                x, y, w, h = boxes[i]
                label = str(classes[class_ids[i]])
                color = colors[i%80]
                if self.task=="Human Detection":
                    if label=="person" or label=="Person":
                        cv2.rectangle(yolo_img, (x,y), (x+w, y+h), color, 2)
                        cv2.putText(yolo_img, label, (x, y - 5), font, 1, color, 1)
                
                else:
                    cv2.rectangle(yolo_img, (x,y), (x+w, y+h), color, 2)
                    cv2.putText(yolo_img, label, (x, y - 5), font, 1, color, 1)
                    
                
                
                
                
        return yolo_img
    
    
    def tinyYOLO(self,frame,configFile,modelFile,labelsfile="" ):
        classes=[]
        net_tiny_yolo=self.net_tiny_yolo
        with open(labelsfile, "r") as f:
            classes = [line.strip() for line in f.readlines()]
            layer_names = net_tiny_yolo.getLayerNames()
            output_layers = [layer_names[i[0] - 1] for i in net_tiny_yolo.getUnconnectedOutLayers()]
            colors = np.random.uniform(0, 255, size=(len(classes), 3))
            
            font = cv2.FONT_HERSHEY_PLAIN
            height, width, channels = frame.shape
            blob = cv2.dnn.blobFromImage(frame, 0.00392, (416, 416), (0, 0, 0), True, crop=False)
            net_tiny_yolo.setInput(blob)
            outs = net_tiny_yolo.forward(output_layers)
            
            class_ids = []
            confidences = []
            boxes = []
            for out in outs:
                for detection in out:
                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]
                    if confidence > 0.2:
                        # Object detected
                        center_x = int(detection[0] * width)
                        center_y = int(detection[1] * height)
                        w = int(detection[2] * width)
                        h = int(detection[3] * height)

                        # Rectangle coordinates
                        x = int(center_x - w / 2)
                        y = int(center_y - h / 2)
                        
                        boxes.append([x, y, w, h])
                        confidences.append(float(confidence))
                        class_ids.append(class_id)

            indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.4, 0.3)

            for i in range(len(boxes)):
                if i in indexes:
                    x, y, w, h = boxes[i]
                    label = str(classes[class_ids[i]])
                    confidence = confidences[i]
                    color = colors[class_ids[i]]
                    if self.task=="Human Detection":
                        if label=="person" or label=="Person":
                        
                            cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
                            cv2.rectangle(frame, (x, y), (x + w, y + 30), color, -1)
                            cv2.putText(frame, label + " " + str(round(confidence, 2)), (x, y + 30), font, 3, (255,255,255), 3)
                    else:
                        cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
                        cv2.rectangle(frame, (x, y), (x + w, y + 30), color, -1)
                        cv2.putText(frame, label + " " + str(round(confidence, 2)), (x, y + 30), font, 3, (255,255,255), 3)
                        
        return frame   
    
    def YOLOv2(self,frame,configFile,modelFile,labelsfile="" ):
        yolo_img=frame.copy()
        net_yolov2=self.net_yolov2
        net_yolov2.setPreferableBackend(cv2.dnn.DNN_BACKEND_DEFAULT)
        net_yolov2.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
        min_confidence=0.14
        with open(labelsfile, 'rt') as f:
            classes = f.read().rstrip('\n').split('\n')
    

        height,width,ch=frame.shape
        blob = cv2.dnn.blobFromImage(yolo_img, 1.0/255.0, (416, 416), True, crop=False)
        net_yolov2.setInput(blob)
        predictions = net_yolov2.forward()
        probability_index=5
        bboxes = []
        for i in range(predictions.shape[0]):
            prob_arr=predictions[i][probability_index:]
            class_index=prob_arr.argmax(axis=0)
            confidence= prob_arr[class_index]
            if confidence > min_confidence:
                x_center=predictions[i][0]*width
                y_center=predictions[i][1]*height
                width_box=predictions[i][2]*width
                height_box=predictions[i][3]*height
                x1=int(x_center-width_box * 0.5)
                y1=int(y_center-height_box * 0.5)
                x2=int(x_center+width_box * 0.5)
                y2=int(y_center+height_box * 0.5)
                
                bboxes.append([x1, y1, x2, y2])
                if self.task=="Human Detection":
                    if classes[class_index]=="person" or classes[class_index]=="Person":
                        cv2.rectangle(yolo_img,(x1,y1),(x2,y2),(255,255,255),1)
                        cv2.putText(yolo_img,classes[class_index]+" "+"{0:.1f}".format(confidence),(x1,y1), cv2.FONT_HERSHEY_SIMPLEX, 1,(255,255,255),1,cv2.LINE_AA)
                else:
                    cv2.rectangle(yolo_img,(x1,y1),(x2,y2),(255,255,255),1)
                    cv2.putText(yolo_img,classes[class_index]+" "+"{0:.1f}".format(confidence),(x1,y1), cv2.FONT_HERSHEY_SIMPLEX, 1,(255,255,255),1,cv2.LINE_AA)
            
             
        
        return yolo_img
        
        
    def MotionDetection(self,frame):

        global fgmask,fgbgAdaptiveGaussain,fgbgBayesianSegmentation,kernel
        if self.model1_name=='Background Subtractor':
            fgmask = self.fgbg.apply(frame)
            ret_img=fgmask
            
        if self.model1_name=='Adaptive Gaussian':
            fgbgAdaptiveGaussainmask = self.fgbgAdaptiveGaussain.apply(frame)
            ret_img=fgbgAdaptiveGaussainmask
            
        if self.model1_name=='Bayesian Segmentation':
            fgbgBayesianSegmentationmask = self.fgbgBayesianSegmentation.apply(frame)
            fgbgBayesianSegmentationmask = cv2.morphologyEx(fgbgBayesianSegmentationmask,cv2.MORPH_OPEN,self.kernel)
            ret_img=fgbgBayesianSegmentationmask
            
            
            
        return ret_img

	
        
	
    
    

class Thread_Model2(QThread):
    changePixmap = pyqtSignal(QImage)
    global video_source
    f=0;
    valueChanged = QtCore.pyqtSignal(int)

    processFinished=QtCore.pyqtSignal(int)
    fps=    QtCore.pyqtSignal(float)
    time=    QtCore.pyqtSignal(float)
    

    def run(self):
        
        
        framecount = 0
    
        global isPaused,analysisrate,Task,model2_Name,model2_configPath,model2_filePath,video_writer2
        self.task=Task
        self.model2_name=model2_Name
       
        
        self.capture = cv2.VideoCapture(video_source)
        #print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {}".format(self.capture.get(cv2.CAP_PROP_FPS)))
        #print(self.capture.get(cv2.CAP_PROP_FRAME_COUNT))
        #starttime = time.time()
        ret, self.frame = self.capture.read()
        video_writer2=cv2.VideoWriter('output-{}-{}-{}.avi'.format(model2_Name,str(video_source).split('/')[-1],int(analysisrate)),cv2.VideoWriter_fourcc('M','J','P','G'),int(analysisrate), (self.frame.shape[1],self.frame.shape[0]))
        fvs = FileVideoStream(video_source).start()
        #time.sleep(1.0)
        self.initializeModel()
        fps = FPS().start()
        while self.capture.isOpened() and fvs.running():
            self.processFinished.emit(0)
            frame = fvs.read()
            if frame is None:
                
                break
            #frame = imutils.resize(frame, width=450)
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame = np.dstack([frame, frame, frame])
            totalFrames =self.capture.get(cv2.CAP_PROP_FRAME_COUNT)
            ret, self.frame = self.capture.read(framecount)
            if framecount <= int(self.capture.get(cv2.CAP_PROP_FRAME_COUNT)):
                self.capture.set(cv2.CAP_PROP_POS_FRAMES, framecount)
                success, self.frame = self.capture.read()

                framecount += int(analysisrate)
                rgbImage = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                self.ret_img2=rgbImage
                    
                    
                    
                    
                
                    
                self.ret_img2=self.Analyze_model2(rgbImage)
                        
                    
                    
                
                
                bgrImage=   cv2.cvtColor(self.ret_img2, cv2.COLOR_RGB2BGR)
                video_writer2.write(bgrImage)
                convertToQtFormat = QImage(self.ret_img2.data, w, h, bytesPerLine, QImage.Format_RGB888)
                p = convertToQtFormat.scaled(640, 480)#Qt.KeepAspectRatio
                    
                
                self.changePixmap.emit(p)
                
                #print(nextFrameNo)
                
                #print(totalFrames)
                
                    
                complete = 100*(framecount/totalFrames)
                
                self.valueChanged.emit(complete)
                    
                k = cv2.waitKey(0)
                if k == 27:  
                    cv2.destroyAllWindows()
                        
                        
            cv2.waitKey(1)
            fps.update()
            
            
        fps.stop()
        self.processFinished.emit(100)
        global fps_2,time_2
        fps_2=fps.fps()
        time_2=fps.elapsed()
        self.fps.emit(fps_2)
        self.time.emit(time_2)
        print("[INFO] elasped time: {:.2f} (Model 2)".format(fps.elapsed()))
        print("[INFO] approx. FPS: {:.2f} (Model 2)".format(fps.fps()))
        cv2.destroyAllWindows()
        fvs.stop()
                    

            
            
    def initializeModel(self):
        global isPaused,analysisrate,Task,model1_Name,model1_configPath,model1_filePath,model2_Name,model2_configPath,model2_filePath,model1_labels_file
        #print(model1_Name)
        file_path=model2_filePath
        configFile=model2_configPath
        modelFile=model2_filePath
        
        if model2_Name=="Haar Cascade":
            
            self.faceCascade=cv2.CascadeClassifier(file_path)
            
            
        if model2_Name=="MMOD Dlib" :
           
            self.detector_dlib_mmod=dlib.cnn_face_detection_model_v1(file_path)
            
        if  model2_Name=="HOG Dlib":    
            
            self.detector_dlib_hog=dlib.get_frontal_face_detector()
            
            
        if model2_Name=="Resnet SSD" or model2_Name=="MTCNN" or model2_Name=="Mobile Net SSD" or  model2_Name=="Google Net":
       
            self.net=cv2.dnn.readNetFromCaffe(configFile, modelFile)

            
        if model2_Name=="YOLO v3" :
            
            self.net_yolov3 = cv2.dnn.readNet(modelFile, configFile)
            
        if model2_Name=="Tiny YOLO":
            
            self.net_tiny_yolo= cv2.dnn.readNet(modelFile,configFile )

           
        if model2_Name=="YOLO v2" :
            
            
            self.net_yolov2=cv2.dnn.readNetFromDarknet(configFile, modelFile)
        if model2_Name=="Background Subtractor":
            
            self.fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()

        if model2_Name=="Adaptive Gaussian" :
           
            self.fgbgAdaptiveGaussain = cv2.createBackgroundSubtractorMOG2()
        if model2_Name=="Bayesian Segmentation":
            global kernel,fgbgBayesianSegmentation
                    
            self.kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
            self.fgbgBayesianSegmentation = cv2.bgsegm.createBackgroundSubtractorGMG()
            
            
            
               
                    
    def Analyze_model2(self,img):
        
        ret_img=img
        
        global isPaused,analysisrate,Task,model1_Name,model1_configPath,model1_filePath,model2_Name,model2_configPath,model2_filePath,model1_labels_file
        #print(model1_Name)
        if model2_Name=="Haar Cascade":
            
            
            
            
            ret_img=self.Haar_Cascade(img,file_path=model2_filePath)
            
        if model2_Name=="MMOD Dlib" or model2_Name=="HOG Dlib":
            ret_img=self.Dlib(img,file_path=model2_filePath)
            
            
        if model2_Name=="Resnet SSD":
            ret_img=self.Caffe(img,configFile=model2_configPath,modelFile=model2_filePath)
        if model2_Name=="MTCNN":
            ret_img=self.Caffe(img,configFile=model2_configPath,modelFile=model2_filePath)
            
        if model2_Name=="Mobile Net SSD":
            ret_img=self.Caffe(img,configFile=model2_configPath,modelFile=model2_filePath)
            
        if model2_Name=="Google Net":
            ret_img=self.Caffe(img,configFile=model2_configPath,modelFile=model2_filePath,labelsfile=model2_labels_file)
            
        if model2_Name=="YOLO v3":
            ret_img=self.YOLOv3(img,configFile=model2_configPath,modelFile=model2_filePath,labelsfile=model2_labels_file)
        if model2_Name=="YOLO v2" :
             ret_img=self.YOLOv2(img,configFile=model2_configPath,modelFile=model2_filePath,labelsfile=model2_labels_file)
        if model2_Name=="Tiny YOLO" :
             ret_img=self.tinyYOLO(img,configFile=model2_configPath,modelFile=model2_filePath,labelsfile=model2_labels_file)
        if  model2_Name=="Background Subtractor" or model1_Name=="Adaptive Gaussian" or model1_Name=="Bayesian Segmentation":
            ret_img=self.MotionDetection(img) 
      
    
            
        return ret_img
  
    

                
                
    def Haar_Cascade(self,img,file_path):
        
        #modelfilee='/Users/aniketpihu/ml-workbench-v2/Models/Face_Detection/models/haarcascade_frontalface_default.xml'
        
        
         
        after_image=img.copy()
        gray=cv2.cvtColor((after_image),cv2.COLOR_BGR2GRAY)
        faces=self.faceCascade.detectMultiScale(gray,1.2,5,minSize=(90,90))

        for(x,y,w,h) in faces:
            cv2.rectangle(after_image,(x,y),(x+w,y+h),(255,0,0),2)

        return after_image
    
    
    def Dlib(self, img, inHeight=300, inWidth=0,file_path=""):
        mymodel=""
        
        if self.model2_name=="MMOD Dlib":
            detector=self.detector_dlib_mmod
     
            
        if self.model2_name=="HOG Dlib":
            mymodel="HOG Dlib"
            detector=dlib.get_frontal_face_detector()

        frameDlib = img.copy()
        frameHeight = frameDlib.shape[0]
        frameWidth = frameDlib.shape[1]
        if not inWidth:
            inWidth = int((frameWidth / frameHeight)*inHeight)

        scaleHeight = frameHeight / inHeight
        scaleWidth = frameWidth / inWidth

        frameDlibSmall = cv2.resize(frameDlib, (inWidth, inHeight))

        frameDlibSmall = cv2.cvtColor(frameDlibSmall, cv2.COLOR_BGR2RGB)
        
        faceRects = detector(frameDlibSmall, 0)

        #print(frameWidth, frameHeight, inWidth, inHeight)
        bboxes = []
        if mymodel=="HOG Dlib":
            for faceRect in faceRects:
                cvRect = [int(faceRect.left()*scaleWidth), int(faceRect.top()*scaleHeight),
                      int(faceRect.right()*scaleWidth), int(faceRect.bottom()*scaleHeight) ]
                bboxes.append(cvRect)
                #print(bboxes)
                cv2.rectangle(frameDlib, (cvRect[0], cvRect[1]), (cvRect[2], cvRect[3]), (255, 0, 0),int(round(frameHeight/150)), 2)
                
                
                
        else:
                
                for faceRect in faceRects:
                    cvRect = [int(faceRect.rect.left()*scaleWidth), int(faceRect.rect.top()*scaleHeight),
                              int(faceRect.rect.right()*scaleWidth), int(faceRect.rect.bottom()*scaleHeight) ]
                    bboxes.append(cvRect)
                    #print(bboxes)
                    cv2.rectangle(frameDlib, (cvRect[0], cvRect[1]), (cvRect[2], cvRect[3]), (255, 0, 0),int(round(frameHeight/150)), 2)
        
        
        
        
        return frameDlib
    
    
     
    
    
    
    
    def Caffe(self,frame,configFile,modelFile,labelsfile="" ):
        net=self.net
        #if (self.task=="Face Detection" and (self.model1_name=="MTCNN" or self.model2_name=="MTCNN")):
        # initialize the list of class labels MobileNet SSD was trained to
        # detect, then generate a set of bounding box colors for each class
        CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
                   "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
                   "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
                   "sofa", "train", "tvmonitor"]
        COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

        
        
        frameOpencvDnn = frame.copy()
        frameHeight = frameOpencvDnn.shape[0]
        frameWidth = frameOpencvDnn.shape[1]
        detected_objects = []
        
        if self.model2_name=='MTCNN':
            blob = cv2.dnn.blobFromImage(frameOpencvDnn, 1.0, (300, 300), [104, 117, 123], False, False)
            net.setInput(blob)
            detections = net.forward()
            bboxes = []
            for i in range(detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                if confidence > 0.9:
                    x1 = int(detections[0, 0, i, 3] * frameWidth)
                    y1 = int(detections[0, 0, i, 4] * frameHeight)
                    x2 = int(detections[0, 0, i, 5] * frameWidth)
                    y2 = int(detections[0, 0, i, 6] * frameHeight)
                    bboxes.append([x1, y1, x2, y2])
                    cv2.rectangle(frameOpencvDnn, (x1, y1), (x2, y2), (0, 255, 0), int(round(frameHeight/150)), 8)
                
                
        
        if self.model2_name=='Mobile Net SSD':
           
            blob = cv2.dnn.blobFromImage(cv2.resize(frameOpencvDnn, (300, 300)),0.007843, (300, 300), 127.5)
            net.setInput(blob)
            detections = net.forward()
            for i in np.arange(0, detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                if confidence > 0.3:
                    idx = int(detections[0, 0, i, 1])
                    box = detections[0, 0, i, 3:7] * np.array([frameWidth, frameHeight, frameWidth, frameHeight])
                    (startX, startY, endX, endY) = box.astype("int")
                    label = "{}: {:.2f}%".format(CLASSES[idx],confidence * 100)
                
                    detected_objects.append(label)
                    cv2.rectangle(frameOpencvDnn, (startX, startY), (endX, endY), COLORS[idx], 2)
                                         
                    y = startY - 15 if startY - 15 > 15 else startY + 15
                    if self.task=="Human Detection":
                        if label=="person":
                            cv2.putText(frameOpencvDnn, label, (startX, y),cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                            
                    else:
                        cv2.putText(frameOpencvDnn, label, (startX, y),cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                    print(label)
                                
            
        if self.model2_name=='Google Net':
           
            rows = open(labelsfile).read().strip().split("\n")

            classes=[r[r.find(" ")+1:].split(',')[0] for r in rows]
            blob = cv2.dnn.blobFromImage(frameOpencvDnn, 1.0, (224, 224), (104, 117, 123),False,False)
            net.setInput(blob)
            preds = net.forward()

            idxs = np.argsort(preds[0])[::-1][:5]
            for (i, idx) in enumerate(idxs):
                if i == 0:
                    text = "Label: {}".format(classes[idx])
                    cv2.putText(frameOpencvDnn, text, (5, 25),  cv2.FONT_HERSHEY_SIMPLEX,0.7, (0, 0, 255), 2)
            




                
        return frameOpencvDnn
        

    def YOLOv3(self,frame,configFile,modelFile,labelsfile="" ):
        yolo_img=frame.copy()
        height, width, channels = yolo_img.shape
        
        
        classes = []
        with open(labelsfile, "r") as f:
            classes = [line.strip() for line in f.readlines()]
            
            
        layers_names = self.net_yolov3.getLayerNames()
        output_layers = [layers_names[i[0]-1] for i in self.net_yolov3.getUnconnectedOutLayers()]
        colors = np.random.uniform(0, 255, size=(len(classes), 3))
        
        blob = cv2.dnn.blobFromImage(yolo_img, scalefactor=0.00392, size=(320, 320), mean=(0, 0, 0), swapRB=True, crop=False)
        
        self.net_yolov3.setInput(blob)
        outputs = self.net_yolov3.forward(output_layers)
        boxes = []
        confs = []
        class_ids = []
        for output in outputs:
            for detect in output:
                scores = detect[5:]
                class_id = np.argmax(scores)
                conf = scores[class_id]
                if conf > 0.3:
                    center_x = int(detect[0] * width)
                    center_y = int(detect[1] * height)
                    w = int(detect[2] * width)
                    h = int(detect[3] * height)
                    x = int(center_x - w/2)
                    y = int(center_y - h / 2)
                    boxes.append([x, y, w, h])
                    confs.append(float(conf))
                    class_ids.append(class_id)

        
    
    
    
        indexes = cv2.dnn.NMSBoxes(boxes, confs, 0.5, 0.4)
        font = cv2.FONT_HERSHEY_PLAIN
        for i in range(len(boxes)):
            if i in indexes:
                x, y, w, h = boxes[i]
                label = str(classes[class_ids[i]])
                color = colors[i%80]
                if self.task=="Human Detection":
                    if label=="person" or label=="Person":
                        cv2.rectangle(yolo_img, (x,y), (x+w, y+h), color, 2)
                        cv2.putText(yolo_img, label, (x, y - 5), font, 1, color, 1)
                
                else:
                    cv2.rectangle(yolo_img, (x,y), (x+w, y+h), color, 2)
                    cv2.putText(yolo_img, label, (x, y - 5), font, 1, color, 1)
                    
                
                
                
                
        return yolo_img
    
    
    def tinyYOLO(self,frame,configFile,modelFile,labelsfile="" ):
        classes=[]
        net_tiny_yolo=self.net_tiny_yolo
        with open(labelsfile, "r") as f:
            classes = [line.strip() for line in f.readlines()]
            layer_names = net_tiny_yolo.getLayerNames()
            output_layers = [layer_names[i[0] - 1] for i in net_tiny_yolo.getUnconnectedOutLayers()]
            colors = np.random.uniform(0, 255, size=(len(classes), 3))
            
            font = cv2.FONT_HERSHEY_PLAIN
            height, width, channels = frame.shape
            blob = cv2.dnn.blobFromImage(frame, 0.00392, (416, 416), (0, 0, 0), True, crop=False)
            net_tiny_yolo.setInput(blob)
            outs = net_tiny_yolo.forward(output_layers)
            
            class_ids = []
            confidences = []
            boxes = []
            for out in outs:
                for detection in out:
                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]
                    if confidence > 0.2:
                        # Object detected
                        center_x = int(detection[0] * width)
                        center_y = int(detection[1] * height)
                        w = int(detection[2] * width)
                        h = int(detection[3] * height)

                        # Rectangle coordinates
                        x = int(center_x - w / 2)
                        y = int(center_y - h / 2)
                        
                        boxes.append([x, y, w, h])
                        confidences.append(float(confidence))
                        class_ids.append(class_id)

            indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.4, 0.3)

            for i in range(len(boxes)):
                if i in indexes:
                    x, y, w, h = boxes[i]
                    label = str(classes[class_ids[i]])
                    confidence = confidences[i]
                    color = colors[class_ids[i]]
                    if self.task=="Human Detection":
                        if label=="person" or label=="Person":
                        
                            cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
                            cv2.rectangle(frame, (x, y), (x + w, y + 30), color, -1)
                            cv2.putText(frame, label + " " + str(round(confidence, 2)), (x, y + 30), font, 3, (255,255,255), 3)
                    else:
                        cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
                        cv2.rectangle(frame, (x, y), (x + w, y + 30), color, -1)
                        cv2.putText(frame, label + " " + str(round(confidence, 2)), (x, y + 30), font, 3, (255,255,255), 3)
                        
        return frame   
    
    def YOLOv2(self,frame,configFile,modelFile,labelsfile="" ):
        yolo_img=frame.copy()
        net_yolov2=self.net_yolov2
        net_yolov2.setPreferableBackend(cv2.dnn.DNN_BACKEND_DEFAULT)
        net_yolov2.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
        min_confidence=0.14
        with open(labelsfile, 'rt') as f:
            classes = f.read().rstrip('\n').split('\n')
    

        height,width,ch=frame.shape
        blob = cv2.dnn.blobFromImage(yolo_img, 1.0/255.0, (416, 416), True, crop=False)
        net_yolov2.setInput(blob)
        predictions = net_yolov2.forward()
        probability_index=5
        bboxes = []
        for i in range(predictions.shape[0]):
            prob_arr=predictions[i][probability_index:]
            class_index=prob_arr.argmax(axis=0)
            confidence= prob_arr[class_index]
            if confidence > min_confidence:
                x_center=predictions[i][0]*width
                y_center=predictions[i][1]*height
                width_box=predictions[i][2]*width
                height_box=predictions[i][3]*height
                x1=int(x_center-width_box * 0.5)
                y1=int(y_center-height_box * 0.5)
                x2=int(x_center+width_box * 0.5)
                y2=int(y_center+height_box * 0.5)
                
                bboxes.append([x1, y1, x2, y2])
                if self.task=="Human Detection":
                    if classes[class_index]=="person" or classes[class_index]=="Person":
                        cv2.rectangle(yolo_img,(x1,y1),(x2,y2),(255,255,255),1)
                        cv2.putText(yolo_img,classes[class_index]+" "+"{0:.1f}".format(confidence),(x1,y1), cv2.FONT_HERSHEY_SIMPLEX, 1,(255,255,255),1,cv2.LINE_AA)
                else:
                    cv2.rectangle(yolo_img,(x1,y1),(x2,y2),(255,255,255),1)
                    cv2.putText(yolo_img,classes[class_index]+" "+"{0:.1f}".format(confidence),(x1,y1), cv2.FONT_HERSHEY_SIMPLEX, 1,(255,255,255),1,cv2.LINE_AA)
            
             
        
        return yolo_img
        
    def MotionDetection(self,frame):

        global fgmask,fgbgAdaptiveGaussain,fgbgBayesianSegmentation,kernel
        if self.model2_name=='Background Subtractor':
            fgmask = self.fgbg.apply(frame)
            ret_img=fgmask
            
        if self.model2_name=='Adaptive Gaussian':
            fgbgAdaptiveGaussainmask = self.fgbgAdaptiveGaussain.apply(frame)
            ret_img=fgbgAdaptiveGaussainmask
            
        if self.model2_name=='Bayesian Segmentation':
            fgbgBayesianSegmentationmask = self.fgbgBayesianSegmentation.apply(frame)
            fgbgBayesianSegmentationmask = cv2.morphologyEx(fgbgBayesianSegmentationmask,cv2.MORPH_OPEN,self.kernel)
            ret_img=fgbgBayesianSegmentationmask
            
            
            
        return ret_img

	
        
       
                     
  

class VideoPlayer1(QWidget):

    def __init__(self, parent=None):
        super(VideoPlayer1, self).__init__(parent)
        

        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)

        btnSize = QSize(16, 16)
        videoWidget = QVideoWidget()

      
        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setFixedHeight(24)
        self.playButton.setIconSize(btnSize)
        self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)
        self.playButton.hide()
        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        self.positionSlider.sliderMoved.connect(self.setPosition)

        self.statusBar = QStatusBar()
        self.statusBar.setFont(QFont("Noto Sans", 7))
        self.statusBar.setFixedHeight(14)

        controlLayout = QHBoxLayout()
        controlLayout.setContentsMargins(0, 0, 0, 0)
        
        controlLayout.addWidget(self.playButton)
        controlLayout.addWidget(self.positionSlider)

        layout = QVBoxLayout()
        layout.addWidget(videoWidget)
        layout.addLayout(controlLayout)
        layout.addWidget(self.statusBar)

        self.setLayout(layout)

        self.mediaPlayer.setVideoOutput(videoWidget)
        self.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.mediaPlayer.positionChanged.connect(self.positionChanged)
        self.mediaPlayer.durationChanged.connect(self.durationChanged)
        self.mediaPlayer.error.connect(self.handleError)
        self.statusBar.showMessage("Ready")



    def playbuttonpressed(self):
        global video_play_source_1
        fileName = video_play_source_1
        print(video_play_source_1)
        if fileName != '':
            self.mediaPlayer.setMedia(
                    QMediaContent(QUrl.fromLocalFile(fileName)))
            self.playButton.setEnabled(True)
            self.statusBar.showMessage(fileName)
            self.play()
        
        
            

    def play(self):
       
        

        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.mediaPlayer.pause()
        else:
            self.mediaPlayer.play()

    def mediaStateChanged(self, state):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState: 
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPlay))
        else:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPause))

    def positionChanged(self, position):
        self.positionSlider.setValue(position)

    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)

    def setPosition(self, position):
        self.mediaPlayer.setPosition(position)

    def handleError(self):
        self.playButton.setEnabled(False)
        self.statusBar.showMessage("Error: " + self.mediaPlayer.errorString()) 

class VideoPlayer2(QWidget):

    def __init__(self, parent=None):
        super(VideoPlayer2, self).__init__(parent)
        

        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)

        btnSize = QSize(16, 16)
        videoWidget = QVideoWidget()

      
        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setFixedHeight(24)
        self.playButton.setIconSize(btnSize)
        self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)
        self.playButton.hide()
        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        self.positionSlider.sliderMoved.connect(self.setPosition)

        self.statusBar = QStatusBar()
        self.statusBar.setFont(QFont("Noto Sans", 7))
        self.statusBar.setFixedHeight(14)

        controlLayout = QHBoxLayout()
        controlLayout.setContentsMargins(0, 0, 0, 0)
        
        controlLayout.addWidget(self.playButton)
        controlLayout.addWidget(self.positionSlider)

        layout = QVBoxLayout()
        layout.addWidget(videoWidget)
        layout.addLayout(controlLayout)
        layout.addWidget(self.statusBar)

        self.setLayout(layout)

        self.mediaPlayer.setVideoOutput(videoWidget)
        self.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.mediaPlayer.positionChanged.connect(self.positionChanged)
        self.mediaPlayer.durationChanged.connect(self.durationChanged)
        self.mediaPlayer.error.connect(self.handleError)
        self.statusBar.showMessage("Ready")


    def playbuttonpressed(self):
        global video_play_source_2
        fileName = video_play_source_2
        if fileName != '':
            self.mediaPlayer.setMedia(
                    QMediaContent(QUrl.fromLocalFile(fileName)))
            self.playButton.setEnabled(True)
            self.statusBar.showMessage(fileName)
            self.play()
        
        
            

    def play(self):
       
        

        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.mediaPlayer.pause()
        else:
            self.mediaPlayer.play()
        
          
    def mediaStateChanged(self, state):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPlay))
        else:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPause))

    def positionChanged(self, position):
        self.positionSlider.setValue(position)

    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)

    def setPosition(self, position):
        self.mediaPlayer.setPosition(position)

    def handleError(self):
        self.playButton.setEnabled(False)
        self.statusBar.showMessage("Error: " + self.mediaPlayer.errorString()) 

       
    


if __name__=='__main__':
    if hasattr(QtCore.Qt, 'AA_EnableHighDpiScaling'):
        QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

    if hasattr(QtCore.Qt, 'AA_UseHighDpiPixmaps'):
        QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True)

    app=QApplication(sys.argv)
    app.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)

    window=MLWB()
    window.setWindowTitle('ML WB')
    window.show()
    sys.exit(app.exec_())
   
    
